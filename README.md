
# Cardápio Digital - Backend

<p align="center">
  <a href="#pre-requisites">Pré-requisitos</a> •
  <a href="#how-to-use">Instalando o projeto</a> •
  <a href="#related">Frontend</a> 
</p>

Essa aplicação foi desenvolvido durante o video [Construindo aplicação Fullstack do ZERO](https://youtu.be/lUVureR5GqI?si=d-fHBagSO6bjX58G) utilizando **Java Spring e Spring MVC** para criação do servidor, **Spring Data JPA** para manipulação e persistência de dados, **Lombok** para geração de boilerplates e Postgres Driver para realizar a conexão com banco de dados Postgress.

<h2 id="pre-requisites">💻 Requisitos</h2>

Para rodar esse projeto você precisa ter o Java instalado na sua máquina.

<h2 id="how-to-use"> 🚀 Instalando o projeto</h2>

Primeiro você deve clonar o repositório,

```bash
# Clone o repositório
$ git clone https://github.com/fernandakipper/crud-java-back

# Acesse-o
$ cd crud-java-back
```


<h2 id="related">🫂 Integração com Frontend</h2>

Para realizar a integração com o Frontend, você pode clonar o projeto e rodar localmente, ou desenvolver você mesmo seguindo o tutorial no Youtube.

👉 [Desenvolvendo o Frontend desse Cardápio Digital com React e Typescript](https://www.youtube.com/watch?v=WHruc3_2z68)

👉 [Link do repositório](https://github.com/Fernanda-Kipper/frontend-cardapio-digital)



# Cardápio Digital - Aplicação Frontend

Este projeto é um simples protótipo de um Cardápio Digital. A aplicação foi desenvolvida usando **React, Typescript e React Query**.

<h1 align="center">
    <img src="public/home.png" width="300"/>
    <img src="public/modal.png" width="300"/>
</h1>

## 💻 Requisitos

Antes de iniciar, você deve ter o Node.js e o NPM instalados em sua máquina.

## 🚀 Instalando

Primeiro, você deve clonar o projeto na sua máquina, para isso você
pode colar o seguinte comando em seu terminal

```bash
git clone https://github.com/Fernanda-Kipper/frontend-cardapio.git
cd frontend-cardapio
```

Para instalar as dependências, execute o seguinte comando:

```bash
npm install
```

Por fim, para executar o projeto basta rodar o seguinte:

```bash
npm run dev
```

## 🔧 Compilação

Para compilar a aplicação para produção, execute o seguinte comando:

```bash
npm run build
```
Isso irá gerar uma versão otimizada da aplicação na pasta `dist`.

## 🫂 Integração com Backend

Para realizar a integração com o Backend, você pode clonar o projeto e rodar localmente, ou desenvolver você mesmo seguindo o tutorial no Youtube.

👉 [Desenvolvendo o Backend com Java Spring](https://www.youtube.com/watch?v=lUVureR5GqI&t=239s)

👉 [Link do repositório](https://github.com/Fernanda-Kipper/backend-cardapio-digital)
