package com.example.cardapio.controllers;

import com.example.cardapio.dto.FoodResponseDTO;
import com.example.cardapio.dto.FoodResquestDTO;
import com.example.cardapio.entity.Food;
import com.example.cardapio.repository.FoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("food")
public class FoodController {

    @Autowired
    private FoodRepository repository;

    @CrossOrigin(origins = "*", allowCredentials = "*")
    @GetMapping
    public List<FoodResponseDTO> getAll(){ //  Atráves de um funil stream, map todos para serem transformados em FoodResponseDTO e passar para list()
        List<FoodResponseDTO> foodList = repository.findAll().stream().map(FoodResponseDTO::new).toList();
        return foodList;
    }

    // se fosse para produção seria somente para o dominio do front end
    // origins = "meudominio"
    @CrossOrigin(origins = "*", allowCredentials = "*")
    @PostMapping
    public void saveFood(@RequestBody FoodResquestDTO data){
        Food foodData = new Food(data);
        repository.save(foodData);
        return;
    }
}
